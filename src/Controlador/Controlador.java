package Controlador;

import Modelo.Recibo;
import Vista.dlgRecibo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Controlador implements ActionListener {

    private dlgRecibo vista;
    private Recibo rec;

    public Controlador(Recibo rec, dlgRecibo vista) {
        this.vista = vista;
        this.rec = rec;

        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.cmbTipo.addActionListener(this);
        vista.btnCancelar.setEnabled(false);
        vista.btnCerrar.setEnabled(false);
        vista.btnGuardar.setEnabled(false);
        vista.btnNuevo.setEnabled(true);
        vista.btnLimpiar.setEnabled(false);
        vista.btnMostrar.setEnabled(false);
        vista.txtCosKilo.enable(false);
        vista.txtDomi.enable(false);
        vista.txtFecha.enable(false);
        vista.txtImpuesto.enable(false);
        vista.txtKiloConsm.enable(false);
        vista.txtNombre.enable(false);
        vista.txtNumRecibo.enable(false);
        vista.txtSubtotal.enable(false);
        vista.txtTotal.enable(false);

    }

    public void Limpiar() {
        vista.txtCosKilo.setText("");
        vista.txtDomi.setText("");
        vista.txtFecha.setText("");
        vista.txtImpuesto.setText("");
        vista.txtKiloConsm.setText("");
        vista.txtNombre.setText("");
        vista.txtNumRecibo.setText("");
        vista.txtSubtotal.setText("");
        vista.txtTotal.setText("");

    }

    private void iniciarVista() {
        vista.setVisible(true);
        vista.setSize(730, 530);
        vista.setTitle("Recibo");
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == vista.btnCancelar) {
            vista.btnCancelar.setEnabled(false);
            vista.btnCerrar.setEnabled(false);
            vista.btnGuardar.setEnabled(false);
            vista.btnNuevo.setEnabled(true);
            vista.btnLimpiar.setEnabled(false);
            vista.btnMostrar.setEnabled(false);
            vista.txtCosKilo.enable(false);
            vista.txtDomi.enable(false);
            vista.txtFecha.enable(false);
            vista.txtImpuesto.enable(false);
            vista.txtKiloConsm.enable(false);
            vista.txtNombre.enable(false);
            vista.txtNumRecibo.enable(false);
            vista.txtSubtotal.enable(false);
            vista.txtTotal.enable(false);
        } else if (e.getSource() == vista.btnCerrar) {
            int respuesta = JOptionPane.showConfirmDialog(vista, "¿Quieres salir?",
                    "Salir", JOptionPane.YES_NO_OPTION);
            if (respuesta == JOptionPane.YES_NO_OPTION) {
                vista.dispose();
                System.exit(0);
            }
        } else if (e.getSource() == vista.btnGuardar) {
            try {

                rec.setCostok(Float.parseFloat(vista.txtCosKilo.getText()));
                rec.setDomicilio(vista.txtDomi.getText());
                rec.setFecha(vista.txtFecha.getText());
                rec.setKiloCon(Float.parseFloat(vista.txtKiloConsm.getText()));
                rec.setNombre(vista.txtNombre.getText());
                rec.setNum_Recibo(Integer.parseInt(vista.txtNumRecibo.getText()));
                rec.setTipo(vista.cmbTipo.getSelectedIndex());
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "Error: Ingresa valores numéricos válidos",
                        "Error de formato", JOptionPane.ERROR_MESSAGE);
            }
        } else if (e.getSource() == vista.cmbTipo) {
            switch (vista.cmbTipo.getSelectedIndex()) {
                case 0:
                    vista.txtCosKilo.setText("2");
                    break;
                case 1:
                    vista.txtCosKilo.setText("3");
                    break;
                case 2:
                    vista.txtCosKilo.setText("5");
                    break;
            }

        } else if (e.getSource() == vista.btnNuevo) {
            vista.btnCancelar.setEnabled(true);
            vista.btnCerrar.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
            vista.btnNuevo.setEnabled(true);
            vista.btnLimpiar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
            vista.txtCosKilo.setEnabled(true);
            vista.txtDomi.setEnabled(true);
            vista.txtFecha.setEnabled(true);
            vista.txtImpuesto.setEnabled(true);
            vista.txtKiloConsm.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtNumRecibo.setEnabled(true);
            vista.txtSubtotal.setEnabled(true);
            vista.txtTotal.setEnabled(true);
        } else if (e.getSource() == vista.btnLimpiar) {
            Limpiar();
        } else if (e.getSource() == vista.btnMostrar) {
            try {
                vista.txtCosKilo.setText(String.valueOf(rec.getCostok()));
                vista.txtImpuesto.setText(String.valueOf(rec.impuesto()));
                vista.txtFecha.setText(rec.getFecha());
                vista.txtKiloConsm.setText(String.valueOf((long) rec.getKiloCon()));
                vista.txtNombre.setText(rec.getNombre());
                vista.txtNumRecibo.setText(String.valueOf(rec.getNum_Recibo()));
                vista.txtSubtotal.setText(String.valueOf(rec.subtotal()));
                vista.txtTotal.setText(String.valueOf((long) rec.total()));
                vista.txtDomi.setText(rec.getDomicilio());
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "Error: Ingresa valores numéricos válidos",
                        "Error de formato", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public static void main(String[] args) {
        Recibo rec = new Recibo();
        dlgRecibo vista = new dlgRecibo(new JFrame(), true);
        Controlador contra = new Controlador(rec, vista);
        contra.iniciarVista();
    }

}
