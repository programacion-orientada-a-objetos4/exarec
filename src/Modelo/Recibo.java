
package Modelo;

/**
 *
 * @author Hp
 */
public class Recibo {
    private String nombre;
    private int tipo;
    private float kiloCon;
    private float Costok;
    private String Fecha;
    private String Domicilio;
    private int num_Recibo;
    
    public Recibo(){
        
    }
    public Recibo(Recibo rec){
        
    }
    public Recibo(String nombre, int tipo, float kiloCon, float Costok, String Fecha, String Domicilio, int num_Recibo) {
        this.nombre = nombre;
        this.tipo = tipo;
        this.kiloCon = kiloCon;
        this.Costok = Costok;
        this.Fecha = Fecha;
        this.Domicilio = Domicilio;
        this.num_Recibo = num_Recibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float getKiloCon() {
        return kiloCon;
    }

    public void setKiloCon(float kiloCon) {
        this.kiloCon = kiloCon;
    }

    public float getCostok() {
        return Costok;
    }

    public void setCostok(float Costok) {
        this.Costok = Costok;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String Fecha) {
        this.Fecha = Fecha;
    }

    public String getDomicilio() {
        return Domicilio;
    }

    public void setDomicilio(String Domicilio) {
        this.Domicilio = Domicilio;
    }

    public int getNum_Recibo() {
        return num_Recibo;
    }

    public void setNum_Recibo(int num_Recibo) {
        this.num_Recibo = num_Recibo;
    }
  public float subtotal() {
    
    int tipoValue = Math.min(Math.max(getTipo(), 0), 2); 

    switch (tipoValue) {
        case 0:
            setCostok(2.0f);
            break;
        case 1:
            setCostok(3.0f);
            break;
        case 2:
            setCostok(5.0f);
            break;
        default:
            break;
    }

    float subtotal = getCostok()*getKiloCon();
    return subtotal;
}


    public float impuesto(){
        return (float) (subtotal()*.16);
    }
    public float total(){
        return subtotal()+impuesto();
    }
}
